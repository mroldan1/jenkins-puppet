# Jenkins imports the Jenkins repository keys and adds the repository to a CentOS host
# installs required packages in order to get a functional installation
class jenkins {

  package { 'wget':
    ensure => latest,
  }

  exec { 'obtain-keys':
    command => 'sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo',
    path    => ['/usr/bin', '/usr/sbin',],
    require => [Package['wget']],
    creates => '/etc/yum.repos.d/jenkins.repo',
  }

  exec { 'import-repo':
    command => 'sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key',
    path    => ['/usr/bin', '/usr/sbin',],
  }

  package { 'java-1.8.0-openjdk.x86_64':
  ensure => 'present',
  }


  package { 'jenkins':
    ensure  => latest,
    require => [Exec['obtain-keys'], Exec['import-repo']],
  }

  service { 'jenkins':
    ensure => 'running',
    require => [Package['jenkins']],
  }

  if $::hostname == 'jenkins-master' {
    exec{'create-plugin-dir':
      command => 'mkdir -p /var/lib/jenkins/plugins',
      path    => ['/usr/bin', '/usr/sbin'],
    }
    exec{'retrieve-swarm-plugin':
      command => 'wget https://updates.jenkins-ci.org/download/plugins/swarm/3.15/swarm.hpi -O /var/lib/jenkins/plugins/swarm.hpi',
      require => [Exec['create-plugin-dir']],
      path    => ['/usr/bin', '/usr/sbin'],
    }

  }
  else {
    file { 'swarm-plugin':
      ensure => 'file',
      path   => '/home/vagrant/swarm-plugin-3.9.jar',
      mode   => 'a+x',
      source => 'https://repo.jenkins-ci.org/releases/org/jenkins-ci/plugins/swarm-client/3.9/swarm-client-3.9.jar',
    }

    exec { 'run-swarm':
      command => 'java -jar /home/vagrant/swarm-plugin.jar -master http://jenkins-master.org:8080/ -fsroot /root/jenkins/ -mode normal -username admin -password admin -name jenkins-slave.org',
      path    => ['/usr/bin'],
      require => [File['swarm-plugin']],
    }
  }

}
