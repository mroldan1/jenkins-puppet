# Description

POC made to put some work on the theory reviewed in the past week.

## Technologies to be used
* Vagrant
* Puppet
* Bash
* Jenkins
* Gradle (to some extent within the jenkins pipeline as code)
* Artifactory

# Goals

The main goal of this project is to bootstrap a functional environment with a single command, using Vagrant for infrastructure automation, Bash/Puppet for provisioning/setup and Jenkins/Artifactory to play around with some pipelines and deploy sample code.

# TODO:
* <s>Provision environment with Vagrant</s>
* <s>Setup jenkins master and slave with puppet</s>
* <s>Write a shell script to do initial puppet provisioning and setup (master/slave architecture)</s>
* <s>Write a multimachine Vagrantfile</s>
* Automate the initial Jenkins setup
* Run a build job on slave node from the Jenkins master (Using gradle for builds and groovy for pipeline DSL)
* Set up OSS artifactory node