node 'jenkins-master.org', 'jenkins-slave.org' {
  include ::jenkins
}

node default {
  include ntp
  class { 'ntp':
    servers  => [ 'ntp1.corp.com', 'ntp2.corp.com' ],
  }
}
