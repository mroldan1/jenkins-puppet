#!/bin/bash

baseDir="/etc/puppetlabs/puppet"
envDir="/etc/puppetlabs/code/environments/production"
binDir="/opt/puppetlabs/puppet/bin"

setHostsFile() {
    echo "--------- Adding DNS entries to /etc/hosts ---------"
    tee -a /etc/hosts << END
    172.21.9.9 puppet-master.org
    172.21.9.9 puppet
    172.21.9.10 jenkins-master.org
    172.21.9.11 artifactory-node.org
    172.21.9.12 jenkins-slave.org
END
}

setupAutosign() {
    # Creating the autosign file.
    echo "--------- Creating the autosign file. ---------"
    touch "$baseDir"/autosign.conf

    # If the file is created successfuly, proceeds to add the domain entry.
    if [[ $? -eq 0 ]] ; then
        echo "--------- Adding domain entry to autosign.conf ---------"
        echo "*.org" >> "$baseDir"/autosign.conf
        if [[ $? -eq 0 ]] ; then
            echo "--------- Autosign entry added succesfuly. ---------"
        fi
    else
        echo "*** There was a problem creating the autosign file. ***"
    fi
}

setupPuppetAgent() {
    echo "--------- Adding EPEL7 repo ---------"
    rpm -Uvh https://yum.puppet.com/puppet6/puppet6-release-el-7.noarch.rpm
    if [[ $? -ne 0 ]] ; then
        echo "There was a problem adding the repository"; exit 1
    fi

    # Checking if there's a puppet directory.
    if [ ! -f "$binDir"/puppet ] ; then
        echo "--------- Installing puppet agent ---------"
        yum install puppet-agent -y
        if [[ $? -ne 0 ]] ; then
            echo "There was a problem installing the puppet-agent, exiting"; exit 1
        fi
    fi

    echo "--------- Registering node to puppetmaster and applying existing catalog ---------"
    "$binDir"/puppet agent -tv
    if [[ $? -eq 0 ]] ; then
        echo "--------- Node successfuly registered. ---------"
    fi

    # Starts puppet service and enables it.
    echo "--------- Starting puppet service and enabling it. ---------"
    "$binDir"/puppet resource service puppet ensure=running enable=true
    if [[ $? -eq 0 ]] ; then
        echo "--------- Puppet service started successfuly. ---------"
    fi 
    
    tee -a "$baseDir"/puppet.conf << END
    [main]
    certname=$(hostname)
    server=puppet-master.org
    runinterval=1h
END
}

setupPuppetMaster() {
    echo "--------- Installing puppet server ---------"

    echo "--------- Adding EPEL7 repo ---------"
    rpm -Uvh https://yum.puppet.com/puppet6/puppet6-release-el-7.noarch.rpm
    if [[ $? -ne 0 ]] ; then
        echo "There was a problem adding the repository, exiting"; exit 1
    fi

    # Checking if there's a puppet directory.
    if [ ! -f "$binDir"/puppet ] ; then
        yum install puppetserver -y
        if [[ $? -ne 0 ]] ; then
            echo "There was a problem installing the puppetserver package, exiting"; exit 1
        fi
    fi

    # Checking if service is down
    # Starts the service and enables it to start on boot.
    if [[ $(systemctl show -p ActiveState puppetserver | cut -d'=' -f2) != "active" ]] ; then
        echo "--------- Starting puppetserver service ---------"
        systemctl start puppetserver
        if [[ $? -eq 0 ]] ; then
            echo "--------- Puppetserver started successfuly. ---------"
        fi
        echo "--------- Enabling puppetserver service ---------"
        systemctl enable puppetserver
        if [[ $? -eq 0 ]] ; then
            echo "--------- Puppetserver service has been enabled. ---------"
        fi
    fi

    if [[ ! -d "$envDir"/modules/jenkins ]] ; then
        echo "--------- Copying manifests to puppet production environment ---------"
        cp /vagrant/manifests/init.pp "$envDir"/manifests/init.pp
        cp -r /vagrant/modules/* "$envDir"/modules/
        if [[ $? -eq 0 ]] ; then
            echo "--------- Successfuly copied manifests ---------"
        fi
    fi
}

if [[ $(hostname) == "puppet-master.org" ]] ; then
    setHostsFile
    setupPuppetMaster
    setupAutosign
else 
    setHostsFile
    setupPuppetAgent
fi