# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
    config.vm.define "puppet-master" do |puppetMaster|
        puppetMaster.vm.box = "centos/7"
        puppetMaster.vm.boot_timeout = 300
        puppetMaster.vm.hostname = "puppet-master.org"

        puppetMaster.vm.network :private_network, 
            ip: "172.21.9.9", 
            netmask: "255.255.240.0", 
            hostsupdater: "skip"
        puppetMaster.vm.network :forwarded_port, guest: 8040, host: 8040
    
        puppetMaster.vm.provision "shell", path: "./scripts/bootstrap.sh"
        puppetMaster.vm.provider "virtualbox" do |vb|
            vb.name = "puppet-master"
            vb.memory = "3096"
        end
    end

    config.vm.define "jenkins-master" do |jenkinsMaster|
        jenkinsMaster.vm.box = "centos/7"
        jenkinsMaster.vm.boot_timeout = 300
        jenkinsMaster.vm.hostname = "jenkins-master.org"

        jenkinsMaster.vm.network :private_network, 
            ip: "172.21.9.10", 
            netmask: "255.255.240.0", 
            hostsupdater: "skip"
        jenkinsMaster.vm.network :forwarded_port, guest: 8080, host: 8080
        
        jenkinsMaster.vm.provision "shell", path: "./scripts/bootstrap.sh"
        jenkinsMaster.vm.provider "virtualbox" do |vb|
          vb.name = "jenkins-master"
          vb.memory = "1024"
        end
      end

      config.vm.define "jenkins-slave" do |jenkinsSlave|
        jenkinsSlave.vm.box = "centos/7"
        jenkinsSlave.vm.boot_timeout = 300
        jenkinsSlave.vm.hostname = "jenkins-slave.org"

        jenkinsSlave.vm.network :private_network, 
            ip: "172.21.9.12", 
            netmask: "255.255.240.0", 
            hostsupdater: "skip"
        jenkinsSlave.vm.network :forwarded_port, guest: 8080, host: 8085

        jenkinsSlave.vm.provision "shell", path: "./scripts/bootstrap.sh"
        jenkinsSlave.vm.provider "virtualbox" do |vb|
            vb.name = "jenkins-slave"
            vb.memory = "1024"
        end
    end

    config.vm.define "artifactory-node" do |artifactory|
        artifactory.vm.box = "centos/7"
        artifactory.vm.boot_timeout = 300
        artifactory.vm.hostname = "artifactory-node.org"
        artifactory.vm.network :private_network, ip: "172.21.9.11", netmask: "255.255.240.0", hostsupdater: "skip"
        artifactory.vm.network :forwarded_port, guest: 8081, host: 8081
        artifactory.vm.provision "shell", path: "./scripts/bootstrap.sh"
        artifactory.vm.provider "virtualbox" do |vb|
            vb.name = "artifactory-node"
            vb.memory = "1024"
        end
    end

end
